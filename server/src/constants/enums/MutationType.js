const MutationType = {
  Create: 'CREATED',
  Update: 'UPDATED',
  Delete: 'DELETED'
};

module.exports = MutationType;
