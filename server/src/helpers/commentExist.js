const commentExist = async (parent, args, context) => {
  try {
    const comment = await context.prisma.comment({
      id: args.commentId
    });

    if (!comment) {
      throw new Error(`Comment with Id ${args.product} does not exist`);
    }

    return comment;
  } catch {
    throw new Error(`Comment with Id ${args.product} does not exist`);
  }
};

module.exports = commentExist;
