const posts = async (parent, args, context) => {
  const allPosts = await context.prisma.posts({ orderBy: 'createdAt_ASC' });
  return allPosts;
};

module.exports = {
  posts
};
