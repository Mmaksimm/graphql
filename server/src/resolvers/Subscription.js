const MutationType = require('../constants/enums/MutationType');

const newPostSubscribe = (parent, args, context) => context.prisma.$subscribe.post({
  mutation_in: [MutationType.Create]
}).node();

const newPost = {
  subscribe: newPostSubscribe,
  resolve: payload => payload
};

const updatePostSubscribe = (parent, args, context) => context.prisma.$subscribe.post({
  mutation_in: [MutationType.Update]
}).node();

const updatePost = {
  subscribe: updatePostSubscribe,
  resolve: payload => payload
};

const newCommentSubscribe = (parent, args, context) => context.prisma.$subscribe.comment({
  mutation_in: [MutationType.Create]
}).node();

const newComment = {
  subscribe: newCommentSubscribe,
  resolve: payload => payload
};

const updateCommentSubscribe = (parent, args, context) => context.prisma.$subscribe.comment({
  mutation_in: [MutationType.Update]
}).node();

const updateComment = {
  subscribe: updateCommentSubscribe,
  resolve: payload => payload
};

module.exports = {
  newPost,
  updatePost,
  newComment,
  updateComment
};
