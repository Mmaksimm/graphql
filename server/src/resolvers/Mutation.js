/* eslint-disable no-console */
const postExist = require('../helpers/postExist');
const commentExist = require('../helpers/commentExist');

const addPost = async (parent, args, context) => {
  try {
    const post = await context.prisma.createPost({
      text: args.text
    });

    return post;
  } catch {
    throw new Error('Post didn\'t create');
  }
};

const addComment = async (parent, args, context) => {
  try {
    await postExist(parent, args, context);
    const comment = await context.prisma.createComment({
      post: { connect: { id: args.postId } },
      text: args.text
    });
    return { ...comment, post: args.postId };
  } catch {
    throw new Error('Comment didn\'t create');
  }
};

const addPostLike = async (parent, args, context) => {
  try {
    const { like } = await postExist(parent, args, context);
    await context.prisma.updatePost({
      data: {
        like: like + 1
      },
      where: {
        id: args.postId
      }
    });

    return { like: like + 1 };
  } catch {
    throw new Error('Post didn\'t like');
  }
};

const addPostDislike = async (parent, args, context) => {
  try {
    const { dislike } = await postExist(parent, args, context);

    await context.prisma.updatePost({
      data: {
        dislike: dislike + 1
      },
      where: {
        id: args.postId
      }
    });

    return { dislike: dislike + 1 };
  } catch {
    throw new Error('Post didn\'t dislike');
  }
};

const addCommentLike = async (parent, args, context) => {
  try {
    const { like } = await commentExist(parent, args, context);

    await context.prisma.updateComment({
      data: {
        like: like + 1
      },
      where: {
        id: args.commentId
      }
    });

    return { like: like + 1 };
  } catch {
    throw new Error('Comment didn\'t like');
  }
};

const addCommentDislike = async (parent, args, context) => {
  try {
    const { dislike } = await commentExist(parent, args, context);

    await context.prisma.updateComment({
      data: {
        dislike: dislike + 1
      },
      where: {
        id: args.commentId
      }
    });

    return { dislike: dislike + 1 };
  } catch {
    throw new Error('Comment didn\'t dislike');
  }
};

module.exports = {
  addPost,
  addComment,
  addPostLike,
  addPostDislike,
  addCommentLike,
  addCommentDislike
};
