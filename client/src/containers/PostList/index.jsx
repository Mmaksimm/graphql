/* eslint-disable no-undef */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'semantic-ui-react';
import getStartDate from '../../helpers/getStartDate';
import Preloader from '../../components/Preloader';
import ExpandedPost from '../../components/ExpandedPost';
import Divider from '../../components/Divider';

import styles from './styles.module.scss';

const PostList = ({
  posts
}) => {
  let date = null;
  const startNewDate = ({ createdAt }) => {
    if (!date || (getStartDate(createdAt) - getStartDate(date)) > 86400000) {
      date = new Date(createdAt);
      return true;
    }
    return false
  };

  return (
    <Card className={`${styles.card} message-list`}>
      {
        posts?.length
          ? (
            posts
              .map(message => message)
              .sort((messagePrev, message) => (Date.parse(messagePrev.createdAt) - Date.parse(message.createdAt)))
              .map(message => {
                return <Fragment key={message.id}>
                  {startNewDate(message) && <Divider date={message.createdAt} />}
                  <ExpandedPost post={message} />
                </Fragment>
              })
          )
          : <Preloader />
      }
    </Card>
  )
}

PostList.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [PropTypes.string, PropTypes.bool]
      )
    )
  )
};

export default PostList;
