/* eslint-disable no-undef */
import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import { Container } from 'semantic-ui-react';
import PostList from '../PostList'
import Header from '../../components/Header';
import AddComment from '../../components/AddComment';
import Preloader from '../../components/Preloader'
import { POSTS_QUERY, NEW_POST_SUBSCRIPTION } from '../../helpers/queries';
import AddPost from '../../components/AddPost';

import styles from './styles.module.scss';


const Chat = ({ params }) => {
  const [isNewPost, setNewPost] = useState(false)
  const _subscribeToNewPosts = subscribeToMore => {
    subscribeToMore({
      document: NEW_POST_SUBSCRIPTION,
      variables: { post: params },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newPost } = subscriptionData.data;
        const exist = prev.posts.find(({ id }) => id === newPost.id);
        if (exist) return prev;

        return {
          ...prev,
          posts: [...prev.posts, newPost]
        }
      }
    })
  };

  const { loading, data, subscribeToMore } = useQuery(
    POSTS_QUERY,
    { variables: { posts: params } }
  );

  useState(() => {
    _subscribeToNewPosts(subscribeToMore);
  })

  return ((loading || !data?.posts?.length)
    ? <Preloader />
    : <>
      <Container className={`${styles.chart} chart`}>
        <Header messages={data?.posts} />
        < PostList
          posts={data.posts}
        />
        <AddPost />
      </Container>
      {isNewPost &&
        <AddComment
          editModal={isNewPost}
          setNewPost={setNewPost}
          onClick={() => { setNewPost(true) }}
        />
      }
    </>
  )
}

export default Chat;
