import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import {
  createStore,
  applyMiddleware,
  // compose
} from 'redux';

import rootReducer from './containers/Chat/reducer';

const composedEnhancers = composeWithDevTools/* compose */(applyMiddleware(thunk));

const store = createStore(
  rootReducer,
  composedEnhancers
);

export default store;
