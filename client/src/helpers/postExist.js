const postExist = async (parent, args, context) => {
  try {
    const post = await context.prisma.post({
      id: args.postId
    });

    if (!post) {
      throw new Error(`Post with Id ${args.product} does not exist`);
    }
    return post;
  } catch (err) {
    throw new Error(`Post with Id ${args.product} does not exist`);
  }
};

module.exports = postExist;
