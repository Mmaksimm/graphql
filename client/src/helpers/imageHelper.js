export const getUserImgLink = image => (image
  ? image
  : 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png');
