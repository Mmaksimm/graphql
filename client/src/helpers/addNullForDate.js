const addNullForDate = number => (number > 9)
  ? number
  : `0${number}`

export default addNullForDate;
