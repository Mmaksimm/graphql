import { gql } from '@apollo/client';

export const POSTS_QUERY = gql`
  query postsQuery {
    posts {
      id
      text
      createdAt
      like
      dislike
      comments {
        id
        text
        createdAt
        like
        dislike
      }
    }
  }
`;

export const ADD_POST_MUTATION = gql`
  mutation addPostMutation($text: String!) {
    addPost(text: $text) {
      id
      text
      createdAt
      like
      dislike
      comments {
        id
      }
    }
  }
`;

export const ADD_COMMENT_MUTATION = gql`
  mutation addCommentMutation($text: String!, $postId: ID!) {
    addComment(text: $text, postId: $postId){
      id
      text
      createdAt
      like
      dislike
    }
  }
`;

export const ADD_POST_LIKE_MUTATION = gql`
  mutation addPostLikeMutation($postId: ID!) {
    addPostLike(postId: $postId) {
      like
    }
  }
`;

export const ADD_POST_DISLIKE_MUTATION = gql`
  mutation addPostDislikeMutation($postId: ID!) {
    addPostLike(postId: $postId) {
      dislike
    }
  }
`;

export const ADD_COMMENT_LIKE_MUTATION = gql`
  mutation addCommentLikeMutation($commentId: ID!) {
    addCommentLike(commentId: $commentId) {
      like
    }
  }
`;

export const ADD_COMMENT_DISLIKE_MUTATION = gql`
  mutation addCommentDislikeMutation($commentId: ID!) {
    addCommentLike(commentId: $commentId) {
      dislike
    }
  }
`;

export const NEW_POST_SUBSCRIPTION = gql`
  subscription newPostSubscription {
    newPost {
      id
      text
      createdAt
      like
      dislike
      comments {
        id
      }
    }
  }
`;

export const UPDATE_POST_SUBSCRIPTION = gql`
  subscription updatePostSubscription {
    updatePost  {
      id
      text
      createdAt
      like
      dislike
      comments {
        id,
        text,
        createdAt
        like,
        dislike
      }
    }
  }
`;

export const NEW_COMMENT_SUBSCRIPTION = gql`
  subscription newCommentSubscription {
    newComment {
      id
      text
      createdAt
      like
      dislike
    }
  }
`;

export const UPDATE_COMMENT_SUBSCRIPTION = gql`
  subscription updateCommentSubscription {
    updateComment {
      id
      text
      createdAt
      like
      dislike
    }
  }
`;
