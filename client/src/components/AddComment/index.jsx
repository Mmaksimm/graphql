import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { ADD_POST_MUTATION, POSTS_QUERY } from '../../helpers/queries';
import PropTypes from 'prop-types';
import { Button, Icon, Modal, Form } from 'semantic-ui-react'

import styles from './styles.module.scss';

const AddComment = ({
  editModal,
  setNewPost,
}) => {
  const [getText, setText] = useState('')
  const updateStoreAfterAddingPost = (store, newPost) => {
    const data = store.readQuery({ query: POSTS_QUERY });

    data.posts.postList.push(newPost);
    store.writeQuery({
      query: POSTS_QUERY,
      data
    });
  };

  const HandlerOnChange = ({ target: { value } }) => {
    setText(value);
  }

  /*
  const handlerUpdate = () => {
    const text = getText.trim();
    if (!text || getText === message.text) return;
    updatePost({ text: getText, messageId: message.id })
  }
  */
  return (
    <Modal open={editModal} className={`edit-message-modal ${editModal ? 'modal-shown' : ''}`}>
      <Modal.Header className={styles.modalHeader}>Edit your message</Modal.Header>
      <Modal.Content className={styles.modalContent}>
        <Modal.Description>
          <Form >
            <div>
              <
                textarea
                name="body"
                placeholder="What is the news?"
                rows="3"
                className="edit-message-input"
                value={getText}
                onChange={HandlerOnChange}
              />
            </div>
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Mutation
        mutation={ADD_POST_MUTATION}
        variables={{ text: getText.trim() }}
        update={(store, { data: { newPost } }) => {
          updateStoreAfterAddingPost(store, newPost);
        }}
        onCompleted={() => setNewPost(false)}
      >
        {newPostMutation => (
          <Modal.Actions className={styles.modalAction}>
            <Button.Group>
              <Button icon onClick={newPostMutation} className="edit-message-button">
                <Icon name="save" />
              </Button>
              <Button icon onClick={() => { setNewPost(false) }} className="edit-message-close">
                <Icon name="window close" />
              </Button>
            </Button.Group>
          </Modal.Actions>
        )}
      </Mutation>
    </Modal>
  )
}

AddComment.propTypes = {
  editModal: PropTypes.bool.isRequired,
  setNewPost: PropTypes.func.isRequired
}

export default AddComment
