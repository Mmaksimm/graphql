import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import Post from '../Post';
import Comment from '../Comment';

const ExpandedPost = ({
  post
}) => (
  <>
    <br />
    <Post
      post={post}
    />
    <br />
    {post?.comments &&
      <CommentUI.Group style={{ maxWidth: '100%' }}>
        <Header as="h3" dividing>
          Comments
        </Header>
        {post.comments && post.comments
          .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
          .map(comment => (
            <Comment
              key={comment.id}
              comment={comment}
              postId={post.id}
            />
          ))}
      </CommentUI.Group>
    }
  </>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ExpandedPost;
