import React from 'react';
import PropTypes from 'prop-types';
import { Label, Icon, Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Comment = ({
  comment: {
    text,
    like,
    dislike,
    createdAt,
    id: commentId,
  },
  postId
}) => {
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Content className={styles.commentData}>
        <CommentUI.Metadata className={styles.createdData}>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {text}
        </CommentUI.Text>
        <CommentUI.Actions className={styles.actions}>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
          >
            <Icon name="thumbs up" />
            {like}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
          >
            <Icon name="thumbs down" />
            {dislike}
          </Label>
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.shape({
    body: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    like: PropTypes.oneOfType([PropTypes.number.isRequired, PropTypes.string.isRequired]),
    id: PropTypes.string.isRequired,
    dislike: PropTypes.oneOfType([PropTypes.number.isRequired, PropTypes.string.isRequired]),
    postId: PropTypes.string.isRequired,
  })
};

export default Comment;
