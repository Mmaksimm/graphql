
import React, { useState } from 'react';
import { Container, Form, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const AddPost = ({
  addNewMessage
}) => {
  const [getText, setText] = useState('')

  const HandlerOnChange = ({ target: { value } }) => {
    setText(value);
  }

  const handlerAddNewMessage = (event) => {
    event.preventDefault();
    const text = getText.trim();
    if (!text) return
    addNewMessage(text);
    setText('');
  }

  return (
    <Container className={`${styles.newMessage} message-input`}>
      <Form onSubmit={handlerAddNewMessage} className={styles.form}>
        <
          input
          className={`${styles.input} message-input-text`}
          placeholder="New post"
          value={getText}
          onChange={HandlerOnChange}
        />
        <
          Button
          className={`${styles.send} message-input-button`}
          type="submit"
        >
          Send
        </Button>
      </Form>
    </Container>
  )
}

export default AddPost;
