import React from 'react';
import PropTypes from 'prop-types';
import { Card, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({ post }) => {
  const {
    id,
    text,
    like,
    dislike,
    createdAt
  } = post;

  const date = moment(createdAt).fromNow();

  return (
    <Card style={{ width: '100%' }}>
      <Card.Content>
        <Card.Meta>
          <span className="date">
            {id}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {text}
        </Card.Description>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
        >
          <Icon name="thumbs up" />
          {like}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
        >
          <Icon name="thumbs down" />
          {dislike}
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Post;
